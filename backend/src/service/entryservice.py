from flask import current_app
from peewee import JOIN, fn, DoesNotExist

from src.authentication import has_access
from src.db.model.attachment import Attachment
from src.db.model.entry import Entry
from src.db.model.entrychange import EntryChange
from src.db.model.logbook import Logbook
from src.exceptions import AccessException
from src.exceptions import ConflictException
from src.service.attachmentsservice import get_attachments, handle_img_tags, get_attachment_preview
from src.service.entrylockservice import get_entry_lock, cancel_entry_lock
from src.service.logbookservice import get_logbook, check_attributes
from src.utilities import get_user_groups, convert_attributes, strip_tags


def get_entry(entry_id, revision_n=None, _thread=None):
    entry = Entry.get(Entry.id == entry_id)
    if not has_access(logbook_id=entry.logbook_id):
        raise AccessException("You don't have access to this logbook")
    entry.attachments = get_attachments(entry)
    if revision_n is not None:
        test = get_entry_revision(entry, revision_n)
        return test

    if _thread:
        test = entry.entry_thread
        return test

    return entry


def get_entry_revision(entry, version):
    if version == entry.revision_n:
        return entry
    if 0 <= version < entry.revision_n:
        return EntryRevision(entry.changes[version])
    raise EntryChange.DoesNotExist


def get_entry_changes(entry_id):
    entry = Entry.get(Entry.id == entry_id)
    return entry.changes


def create_entry(args, logbook_id, user='', entry_id=None):
    "Creating a new entry"
    logbook = get_logbook(logbook_id)
    args["logbook"] = logbook
    # make sure the attributes are of proper types
    args["attributes"] = check_attributes(logbook, args.get("attributes", {}))

    if entry_id is not None:
        # we're creating a followup to an existing entry
        args["follows"] = entry_id
    if args.get("content") and args["content_type"].startswith("text/html"):
        # extract any inline images as attachments
        args["content"], inline_attachments = \
            handle_img_tags(args["content"], timestamp=args.get("created_at"))
    else:
        inline_attachments = []

    if args.get("follows_id"):
        # don't allow pinning followups, that makes no sense
        args["pinned"] = False
    entry = Entry.create(**args, owner=user)
    for attachment in inline_attachments:
        attachment.entry = entry
        attachment.save()
    return entry


def update_entry(entry_id, ip=None, user='', **args):
    entry = get_entry(entry_id)

    if entry.edit_lock:
        if user != entry.owner:
            raise AccessException("This entry can only be edited by its creator")
    # to prevent overwiting someone else's changes we require the
    # client to supply the "revision_n" field of the entry they
    # are editing. If this does not match the current entry in the
    # db, it means someone has changed it inbetween and we abort.
    if args["revision_n"] != entry.revision_n:
        raise ConflictException("Conflict: Entry {} has been edited since you last loaded it!".format(entry_id))
    # check for a lock on the entry
    try:
        lock = get_entry_lock(entry_id)
    except DoesNotExist:
        # No one locking, we can work on it
        lock = None
    if lock:
        if lock.owned_by_ip == ip:
            cancel_entry_lock(ip, entry_id)
        else:
            raise ConflictException("Conflict: Entry {} is locked by IP {} since {}"
                                    .format(entry_id, lock.owned_by_ip, lock.created_at))

    if args.get("content") and args.get("content_type", entry.content_type).startswith("text/html"):
        args["content"], inline_attachments = handle_img_tags(args["content"])
    else:
        inline_attachments = []

    change = make_change_entry(entry, **args)
    entry.save()
    change.save()
    for attachment in inline_attachments:
        attachment.entry = entry
        attachment.save()
    return entry


def make_change_entry(entry, **data):
    "Update the entry, storing the old values as a change"
    # Note: we don't make db changes in this method, the user
    # must save the entry and change afterwards!
    original_values = {
        attr: getattr(entry, attr)
        for attr, value in data.items()
        if hasattr(entry, attr) and getattr(entry, attr) != value
    }
    # TODO: what should we do if the new data is the same as the old?
    change = EntryChange(entry=entry, changed=original_values)
    for attr in original_values:
        value = data[attr]
        setattr(entry, attr, value)
    # Only update the change timestamp if the edit is "major".
    # Priority just changes the sorting of entries, so if that's
    # the only thing that changed, we don't bump the timestamp.
    # TODO: allow explicitly marking an edit as "minor", not bumping
    # the timestamp.

    if set(original_values.keys()) != {"priority"} and 'no_change_time' not in data:
        # This is a little tricky; in order to make it possible for e.g.
        # a script to sync changes from another system, it's important to
        # be able to set the "last_changed_at" timestamp to whatever.
        if "last_changed_at" in original_values:
            entry.last_changed_at = data["last_changed_at"]
        else:
            # default to using the generated "now" timestamp
            entry.last_changed_at = change.timestamp
    return change


def stripped_content(entry):
    return strip_tags(entry.content)


def converted_attributes(entry):
    "Ensure that the attributes conform to the logbook configuration"
    return convert_attributes(entry.logbook, entry.attributes)


def search_entry(logbook_id=0, followups=False,
                 child_logbooks=False, archived=False,
                 n=None, offset=0,
                 attribute_filter=None, content_filter=None,
                 title_filter=None, author_filter=None,
                 attachment_filter=None, metadata_filter=None,
                 sort_by_timestamp=True):
    # Note: this is all pretty messy. The reason we're building
    # the query as a raw string is that peewee does not (currently)
    # support recursive queries, which we need in order to search
    # through nested logbooks. Cleanup needed!

    user_groups = get_user_groups()
    db_type = current_app.config["DB_TYPE"]

    if db_type == 'postgresql':
        followup_query = (Entry.select(Entry.follows_id, fn.Count(Entry.id).alias('n_followups'),
                                       fn.array_agg(Entry.authors).alias('followup_authors')).where(
            Entry.follows_id.is_null(False)).group_by(Entry.follows_id).alias('followup_query'))
    else:
        # Default is "sqlite"
        followup_query = (Entry.select(Entry.follows_id, fn.Count(Entry.id).alias('n_followups'),
                                       fn.json_group_array(fn.json(fn.ifnull(Entry.authors, "[]"))).alias(
                                           'followup_authors')).where(
            Entry.follows_id.is_null(False)).group_by(Entry.follows_id).alias('followup_query'))

    # SELECT fields

    query = Entry.select(Entry, followup_query.c.n_followups,
                         fn.COALESCE(followup_query.c.follows_id, Entry.id).alias('thread'),
                         fn.COALESCE(Entry.last_changed_at, Entry.created_at).alias('timestamp'),
                         followup_query.c.followup_authors, Logbook.id.alias('logbook_id'),
                         Logbook.name.alias('logbook_name'))

    if followups or any([title_filter, content_filter, author_filter,
                         metadata_filter, attribute_filter, attachment_filter]):
        query.distinct(Entry.id)
    else:
        query.distinct(followup_query.c.follows_id, Entry.id)

    # FROM and JOINS
    query = query.join(Logbook, on=(Logbook.id == Entry.logbook)). \
        join(followup_query, JOIN.LEFT_OUTER, on=(Entry.id == followup_query.c.follows_id))

    if attachment_filter:
        query = query.join(Attachment, on=(Entry.id == Attachment.entry))

    # WHERE Conditions
    if logbook_id and not child_logbooks:
        # We want to search ONLY in the current logbook
        # Make sense only if we don't have a logbook id
        query = query.where(Entry.logbook == logbook_id)
    else:
        # recursive query to find all entries in the given logbook
        # or any of its descendants, to arbitrary depth, and also
        # any high priority ("important") entries in ancestors

        # Add all ancestor logbook recursively
        ancestor_id = []
        AncestorBase = Logbook.alias()
        ancestor_base_case = AncestorBase.select(AncestorBase.id, AncestorBase.parent_id).where(
            AncestorBase.id == logbook_id).cte('base', recursive=True)
        AncestorRTerm = Logbook.alias()
        ancestor_recursive = AncestorRTerm.select(AncestorRTerm.id, AncestorRTerm.parent_id).join(
            ancestor_base_case, on=(AncestorRTerm.id == ancestor_base_case.c.parent_id))
        ancestor_cte = ancestor_base_case.union_all(ancestor_recursive)
        ancestor_id = ancestor_cte.select_from(ancestor_cte.c.id).alias('ancestor_id')

        # Add all children logbooks recursively
        ChildrenBase = Logbook.alias()
        children_base_case = ChildrenBase.select(ChildrenBase.id, -1)

        children_base_case = children_base_case.where(
            (ChildrenBase.id == logbook_id) |
            ((logbook_id == 0) & ChildrenBase.parent.is_null() & ((archived == 1) | (ChildrenBase.archived == 0))))

        children_base_case = children_base_case.cte('base', recursive=True)

        ChildrenRTerm = Logbook.alias()
        children_recursive = ChildrenRTerm.select(ChildrenRTerm.id, ChildrenRTerm.parent_id).join(
            children_base_case,
            on=((ChildrenRTerm.parent_id == children_base_case.c.id) & (
                    (ChildrenRTerm.archived == 0) | archived == True)))
        children_cte = children_base_case.union_all(children_recursive)
        children_id = children_cte.select_from(children_cte.c.id).alias('children_id')

        query = query.where(
            (Entry.logbook.in_(children_id) | (Entry.logbook.in_(ancestor_id) & (Entry.priority > 100))))

        # In the logbook we allow seeing archived logbook if opened directly, this allows to fetch entries if
        # you open an archived logbook directly
        query = query.where((~Logbook.archived) | (Logbook.id == logbook_id))

    # We do a global search so only need to check for permissions and archived status
    query = query.where((Logbook.user_group == '') | Logbook.user_group.in_(user_groups))

    if not archived:
        query = query.where(~Entry.archived)

    # further filters on the results, depending on search criteria
    if content_filter:
        # checking not null shouldn't be needed, but it doesn't hurt
        if db_type == 'postgresql':
            query = query.where(Entry.content.is_null(False) & Entry.content.iregexp(content_filter))
        else:
            query = query.where(Entry.content.is_null(False) & fn.IREGEXP(content_filter, Entry.content))

    if title_filter:
        if db_type == 'postgresql':
            query = query.where(Entry.title.is_null(False) & Entry.title.iregexp(title_filter))
        else:
            query = query.where(Entry.title.is_null(False) & fn.IREGEXP(title_filter, Entry.title))

    if attachment_filter:
        if db_type == 'postgresql':
            query = query.where(Attachment.path.is_null(False) & Attachment.path.iregexp(attachment_filter))
        else:
            query = query.where(Attachment.path.is_null(False) & fn.IREGEXP(attachment_filter, Attachment.path))

    # (raw_query, variables) = query.sql()
    if author_filter:
        # Check on both name AND login
        # It is a bit messy as it needs to extract the authors (array), join with Entry and then search
        # and those operations are slightly different between SQLite and PostgreSQL
        if db_type == 'postgresql':
            author_query = Entry.select(Entry.id, fn.json_array_elements(Entry.authors).alias('author'))
            query = query.join(author_query, on=(Entry.id == author_query.c.id))
            query = query.where(fn.json_extract_path_text(author_query.c.author, fn.variadic('{name}')).iregexp(
                author_filter) | fn.json_extract_path_text(author_query.c.author, fn.variadic('{login}')).iregexp(
                author_filter))
        else:
            """
            Possible alternative:
            #authors_id = Entry.raw("SELECT entry.id as author_id FROM entry, json_each(entry.authors) WHERE IREGEXPP(?, entry.authors) AND (IREGEXPP(?, json_extract(json_each.value, '$.name')) OR IREGEXPP(?, json_extract(json_each.value, '$.login')))", *[author_filter, author_filter, author_filter])
            #ids = []
            #for test in authors_id:
            #    ids.append(test.author_id)
            #query = query.where(Entry.id.in_(ids))
            This is an alternative way to do, pretty much, the same operation in a slightly different way
            """
            authors2 = Entry.authors.children().alias("authors2")
            authors_id = Entry.select(Entry.id.alias("author_id")).from_(Entry, authors2).where(
                (fn.IREGEXP(author_filter, Entry.authors)) & (
                        fn.IREGEXP(author_filter, fn.json_extract(authors2.c.value, '$.name')) |
                        fn.IREGEXP(author_filter, fn.json_extract(authors2.c.value, '$.login'))
                ))
            query = query.where(Entry.id.in_(authors_id))

    if attribute_filter:
        if db_type == 'postgresql':
            for i, (attr, value) in enumerate(attribute_filter):
                query = query.where(fn.json_extract_path_text(Entry.attributes, fn.variadic(
                    '{attr_id}'.format(attr_id='{' + attr + '}'))).iregexp(value))
        else:
            for i, (attr, value) in enumerate(attribute_filter):
                query = query.where(
                    fn.IREGEXP(value, fn.json_extract(Entry.attributes, '$.{attr_id}'.format(attr_id=attr))))

    if metadata_filter:
        if db_type == 'postgresql':
            for i, (meta, value) in enumerate(metadata_filter):
                query = query.where(fn.json_extract_path_text(Entry.metadata, fn.variadic(
                    '{meta_id}'.format(meta_id='{' + meta + '}'))).iregexp(value))
        else:
            for i, (meta, value) in enumerate(metadata_filter):
                query = query.where(
                    fn.IREGEXP(value, fn.json_extract(Entry.metadata, '$.{meta_id}'.format(meta_id=meta))))

    if followups or any([title_filter, content_filter, author_filter,
                         metadata_filter, attribute_filter, attachment_filter]):
        query = query.order_by(Entry.id)
    else:
        query = query.where(Entry.follows.is_null())
        query = query.order_by(followup_query.c.follows_id, Entry.id)

    # Just need a simple subquery, but at this point peewee is getting a bit tiring and, being a simple
    # sql piece it should work on both sqlite and postgersql without any issue
    (raw_query, variables) = query.sql()
    raw_query = "SELECT * FROM (" + raw_query + ") as t1 "

    # sort newest first, taking into account the last edit if any
    # TODO: does this make sense? Should we only consider creation date?
    if sort_by_timestamp:
        # query = query.order_by("timestamp")
        raw_query += " ORDER BY t1.priority DESC, {} DESC".format("timestamp")
    else:
        # query = query.order_by(Entry.priority.desc(), Entry.created_at.desc())
        raw_query += " ORDER BY t1.priority DESC, {} DESC".format("t1.created_at")
    variable_char = '?'
    if db_type == 'postgresql':
        variable_char = '%s'

    if n:
        # query = query.limit(n)
        raw_query += " LIMIT " + variable_char
        variables.append(n)
        if offset:
            # query = query.offset(offset)
            raw_query += " OFFSET " + variable_char
            variables.append(offset)

    entries = Entry.raw(raw_query, *variables)

    entries_dict = {}
    for entry in entries:
        entries_dict[entry.id] = entry
        entry.logbook_data = {
            "id": entry.logbook_id,
            "name": entry.logbook_name,
        }
        entry.timestamp = entry.last_changed_at or entry.created_at

    attachment_previews = get_attachment_preview(list(entries_dict.keys()))

    for attachment in attachment_previews:
        entry_id = attachment.attachment.e_id
        entries_dict[entry_id].attachment_preview = attachment
        entries_dict[entry_id].n_attachments = attachment.attachment.n_attachments

    return entries


class EntryRevision:
    """An object that represents a historical version of an entry. It
    can (basically) be used like an Entry object."""

    def __init__(self, change):
        self.change = change

    def __getattr__(self, attr):
        # Fetch the old logbook if it was changed, the authorization is still made on the new logbook
        if attr == "logbook":
            if 'logbook_id' in self.change.changed:
                return get_logbook(self.change.changed['logbook_id'])
        if attr == "id":
            return self.change.entry.id
        if attr == "revision_n":
            return list(self.change.entry.changes).index(self.change)
        if attr in ("logbook_id", "title", "authors", "content", "attributes",
                    "metadata", "follows_id", "tags", "archived"):
            return self.change.get_old_value(attr)
        if attr == "converted_attributes":
            return convert_attributes(self.change.entry.logbook,
                                      self.change.get_old_value("attributes"))
        return getattr(self.change.entry, attr)