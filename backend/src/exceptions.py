

class AccessException(Exception):
    """ Raised if the user has no access right to the resource"""
    def __init__(self, message=None):
        self.data = {"message": message}
        super().__init__(message)


class ConflictException(Exception):
    def __init__(self, message=None):
        self.data = {"message": message}
        super().__init__(message)


class LockedException(Exception):
    pass
