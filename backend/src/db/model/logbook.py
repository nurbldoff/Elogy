from datetime import datetime

from peewee import (CharField, TextField, BooleanField, DateTimeField, ForeignKeyField)

from src.db.db import BaseModel


class Logbook(BaseModel):

    """
    A logbook is a collection of entries, and (possibly) other logbooks.
    """
    created_at = DateTimeField(default=datetime.utcnow)
    last_changed_at = DateTimeField(null=True)
    name = CharField()
    description = TextField(null=True)
    template = TextField(null=True)
    template_content_type = CharField(default="text/html; charset=UTF-8")
    parent = ForeignKeyField("self", null=True, backref="children")
    archived = BooleanField(default=False)
    user_group = CharField()
    owner = CharField()

    @classmethod
    def bind_extra_fields(cls):
        """
        Initialize the JSONField using the set value based on the DB_TYPE
        """
        cls._meta.add_field("attributes", BaseModel.JSONField(default=[]))
        cls._meta.add_field("metadata", BaseModel.JSONField(default={}))

    def __str__(self):
        return "[{}] {}".format(self.id, self.name)

    @property
    def revision_n(self):
        return len(self.changes)

    @property
    def ancestors(self):
        "Return self, parent, grandparent, ..."
        AncestorBase = Logbook.alias()
        ancestor_base_case = AncestorBase.select(AncestorBase.id, AncestorBase.parent_id).where(
            AncestorBase.id == self.id).cte('base', recursive=True)
        AncestorRTerm = Logbook.alias()
        ancestor_recursive = AncestorRTerm.select(AncestorRTerm.id, AncestorRTerm.parent_id).join(
            ancestor_base_case, on=(AncestorRTerm.id == ancestor_base_case.c.parent_id))
        ancestor_cte = ancestor_base_case.union_all(ancestor_recursive)
        ancestor_id = ancestor_cte.select_from(ancestor_cte.c.id).alias('ancestor_id')

        # Fetch the logbooks
        query = Logbook.select().where(Logbook.id.in_(ancestor_id))
        return query

    @property
    def descendants(self, archived=False):
        "Return all children, grandchildren, etc of the logbook"
        ChildrenBase = Logbook.alias()
        children_base_case = ChildrenBase.select(ChildrenBase.id, -1)

        children_base_case = children_base_case.where(
            (ChildrenBase.id == self.id) |
            ((self.id == 0) & ChildrenBase.parent.is_null() & ((archived == 1) | (ChildrenBase.archived == 0))))

        children_base_case = children_base_case.cte('base', recursive=True)

        ChildrenRTerm = Logbook.alias()
        children_recursive = ChildrenRTerm.select(ChildrenRTerm.id, ChildrenRTerm.parent_id).join(
            children_base_case,
            on=((ChildrenRTerm.parent_id == children_base_case.c.id) & ((ChildrenRTerm.archived == 0) | archived == True)))
        children_cte = children_base_case.union_all(children_recursive)
        children_id = children_cte.select_from(children_cte.c.id).alias('children_id')

        # Fetch the logbooks
        query = Logbook.select().where(Logbook.id.in_(children_id))

        return query
