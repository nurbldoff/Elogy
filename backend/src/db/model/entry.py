from datetime import datetime

from peewee import (IntegerField, CharField, TextField, BooleanField, DateTimeField, ForeignKeyField, DoesNotExist, fn)

from src.db.db import BaseModel
from src.db.model.logbook import Logbook


class Entry(BaseModel):
    logbook = ForeignKeyField(Logbook, backref="entries")
    title = CharField(max_length=500, null=True)
    content = TextField(null=True)
    # If using PostgreSQL might be useful to have a an index here like
    # CREATE INDEX entry_content_idx ON public.entry using gin ("content" gin_trgm_ops);
    content_type = CharField(default="text/html; charset=UTF-8")
    priority = IntegerField(default=0)  # used for sorting
    # Priority is used for sorting; it takes precedence over timestamp.
    # Currently, there are three priority levels that change behavior
    # 0 = normal
    # 100 = pinned  - sorted before normal entries
    # 200 = important  - sorted before pinned, and shown in descendant
    #                    logbooks.
    created_at = DateTimeField(default=datetime.utcnow)
    last_changed_at = DateTimeField(null=True)
    follows = ForeignKeyField("self", null=True, index=True)
    archived = BooleanField(default=False)
    edit_lock = BooleanField(default=False)
    owner = CharField(default="")

    def __str__(self):
        return "[{}] {}".format(self.id, self.title)

    @classmethod
    def bind_extra_fields(cls):
        cls._meta.add_field("authors", BaseModel.JSONField(default=[]))
        cls._meta.add_field("metadata", BaseModel.JSONField(default={}))
        cls._meta.add_field("attributes", BaseModel.JSONField(default={}))
        return

    @property
    def followups(self):
        return Entry.select().where((Entry.follows_id == self.id)).order_by(Entry.id)

    @property
    def revision_n(self):
        return len(self.changes)

    @property
    def entry_thread(self):
        entries = []
        if self.follows:
            entry = Entry.get(Entry.id == self.follows_id)
            while True:
                entries.append(entry)
                if entry.follows_id:
                    try:
                        entry = Entry.get(Entry.id == entry.follows_id)
                    except DoesNotExist:
                        break
                else:
                    break
        if entries:
            return entries[-1]
        return self
