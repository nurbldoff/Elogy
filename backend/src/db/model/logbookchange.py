from datetime import datetime

from peewee import (CharField, TextField, DateTimeField, ForeignKeyField, DoesNotExist, fn)

from src.db.model.logbook import Logbook
from src.db.db import BaseModel


class LogbookChange(BaseModel):

    logbook = ForeignKeyField(Logbook, backref="changes")
    timestamp = DateTimeField(default=datetime.utcnow)
    change_comment = TextField(null=True)
    change_ip = CharField(null=True)

    def __str__(self):
        return "[{}] {}".format(self.id, self.title)

    @classmethod
    def bind_extra_fields(cls):
        """
        Initialize the JSONField using the set value based on the DB_TYPE
        """
        cls._meta.add_field("change_authors", BaseModel.JSONField(null=True))
        cls._meta.add_field("changed", BaseModel.JSONField(null=True))

    def get_old_value(self, attr):
        """Get the value of the attribute at the time of this revision.
        That is, *before* the change happened."""

        # First check if the attribute was changed in this revision,
        # in that case we return that.
        if attr in self.changed:
            return self.changed[attr]
        # Otherwise, check for the next revision where this attribute
        # changed; the value from there must be the current value
        # at this revision.
        try:
            change = (LogbookChange.select()
                      .where((LogbookChange.logbook == self.logbook) & (LogbookChange.id > self.id)))

            if BaseModel.db_type == 'postgresql':
                change = change.where(fn.json_extract_path_text(LogbookChange.changed, fn.variadic('{attr_id}'.format(attr_id='{' + attr + '}'))).is_null(False))
            else:
                change = change.where(fn.json_extract(LogbookChange.changed, '$.{attr_id}'.format(attr_id=attr)).is_null(False))

            change = change.order_by(LogbookChange.id).get()
            return change.changed[attr]
        except DoesNotExist:
            # No later revisions changed the attribute either, so we can just
            # take the value from the current logbook
            return getattr(self.logbook, attr)

    def get_new_value(self, attr):
        """Get the value of the attribute at the time of this revision.
        That is, *before* the change happened."""

        # check for the next revision where this attribute
        # changed; the value from there must be the current value
        # at this revision.
        try:
            change = (LogbookChange.select()
                      .where((LogbookChange.logbook == self.logbook) & (LogbookChange.id > self.id)))

            if BaseModel.db_type == 'postgresql':
                change = change.where(fn.json_extract_path_text(LogbookChange.changed, fn.variadic('{attr_id}'.format(attr_id='{' + attr + '}'))).is_null(False))
            else:
                change = change.where(fn.json_extract(LogbookChange.changed, '$.{attr_id}'.format(attr_id=attr)).is_null(False))

            change = change.order_by(LogbookChange.id).get()
            return change.changed[attr]
        except DoesNotExist:
            # No later revisions changed the attribute, so we can just
            # take the value from the current logbook
            return getattr(self.logbook, attr)
