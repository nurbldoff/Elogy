import re
import sqlite3
import sys

import playhouse
from peewee import Model, DatabaseProxy


db = DatabaseProxy()


class BaseModel(Model):

    JSONField = None
    db_type = None

    class Meta:
        database = db
        order_by = ("id",)


def setup_database(db_type, db_name, db_host=None, db_user=None, db_password=None):
    database = set_db_type(db_type, db_name, db_host, db_user, db_password)
    init_extra_fields()
    db.initialize(database)
    init_database()


def set_db_type(db_type, db_name, db_host, db_user, db_password):
    BaseModel.db_type = db_type
    # Would be nice to support both system if needed
    if db_type == 'postgresql':
        from playhouse.pool import PooledPostgresqlExtDatabase
        from playhouse.postgres_ext import JSONField

        database = PooledPostgresqlExtDatabase(db_name,
                                               host=db_host,
                                               user=db_user,
                                               password=db_password,
                                               max_connections=16,
                                               stale_timeout=300)
        BaseModel.JSONField = JSONField
        return database

    elif db_type == 'sqlite':
        from playhouse.sqlite_ext import SqliteExtDatabase, JSONField

        if sqlite3.sqlite_version_info[:3] < (3, 9, 0):
            sys.exit('Sqlite version too low, 3.9.0 or later required')
        tmp_db = sqlite3.connect(':memory:')
        setup_test_table = 'create table temp(attrib1,attrib2)'
        tmp_db.execute(setup_test_table)
        test_json_ext = (
            'insert into temp (attrib1, attrib2)'
            ' values("first", json(\'{"A":"12345", "B":"54321"}\'))')
        try:
            # Test if query with function using JSON1 works
            tmp_db.execute(test_json_ext)
        except:
            tmp_db.close()
            sys.exit('Could not find SQLite JSON1 extension.')
        finally:
            tmp_db.close()

        BaseModel.JSONField = JSONField
        database = SqliteExtDatabase(db_name, regexp_function=True)
        database.register_function(json_iregex_function, name='IREGEXP', num_params=2)
        return database


def init_extra_fields():
    # TODO should be improved, like automatically call it for every file inside the model package
    from src.db.model.logbook import Logbook
    from src.db.model.logbookchange import LogbookChange
    from src.db.model.entry import Entry
    from src.db.model.entrylock import EntryLock
    from src.db.model.entrychange import EntryChange
    from src.db.model.attachment import Attachment

    Logbook.bind_extra_fields()
    LogbookChange.bind_extra_fields()
    Entry.bind_extra_fields()
    EntryLock.bind_extra_fields()
    EntryChange.bind_extra_fields()
    Attachment.bind_extra_fields()


def init_database():
    from src.db.model.logbook import Logbook
    from src.db.model.logbookchange import LogbookChange
    from src.db.model.entry import Entry
    from src.db.model.entrylock import EntryLock
    from src.db.model.entrychange import EntryChange
    from src.db.model.attachment import Attachment

    Logbook.create_table(safe=True)
    LogbookChange.create_table(safe=True)
    Entry.create_table(safe=True)
    EntryChange.create_table(safe=True)
    EntryLock.create_table(safe=True)
    Attachment.create_table(safe=True)


def json_iregex_function(regexp, json_data):
    if json_data is not None and re.search(str(regexp), str(json_data), re.IGNORECASE):
        return True
    return False
