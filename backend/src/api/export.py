from flask import send_file, send_from_directory
from flask_restful import Resource
from webargs.fields import (Integer, Boolean)
from webargs.flaskparser import use_args

from src.service.exportservice import export_entry_as_html, export_logbook_as_html


class DownloadResource(Resource):
    """Handle request for an entry download"""

    @use_args({"entry_id": Integer(allow_none=True), "logbook_id": Integer(allow_none=True), "include_attachments": Boolean(load_default=False)}, location="query")
    def get(self, args):
        """
        Fetch the logbook or the entry for download.
        From the method before refactoring it would fetch the entry then, if the logbook id was there, it would
        also fetch the logbook and overwrite the work done. So I guess either you don't have both ids at the same
        time, or you simply favor logbook id over the entry one
        """
        html = ""
        attachments = args['include_attachments']
        if "logbook_id" in args:
            html = export_logbook_as_html(args["logbook_id"], attachments)
        elif "entry_id" in args:
            html = export_entry_as_html(args["entry_id"], attachments)

        if attachments:
            return send_file(html, mimetype="application/zip", as_attachment=True, attachment_filename=html)
        else:
            return send_file(html, mimetype="text/html", as_attachment=True, attachment_filename=html)
