from flask import request
from flask_restful import Resource, marshal, marshal_with, abort
from webargs.fields import (Integer, Str, Boolean, Dict, List,
                            Nested, Email, DateTime)
from webargs.flaskparser import use_args

from ..actions import new_entry, edit_entry
from . import fields, send_signal

from src.service.entryservice import get_entry, get_entry_changes, create_entry, update_entry, search_entry
from src.service.entrylockservice import cancel_entry_lock, get_entry_lock
from src.service.logbookservice import get_logbook, get_logbooks
from src.authentication import get_user

entry_args = {
    "id": Integer(allow_none=True),
    "logbook_id": Integer(allow_none=True),
    "title": Str(allow_none=True),
    "content": Str(),
    "content_type": Str(load_default="text/html"),
    "authors": List(Nested({
        "name": Str(),
        "login": Str(allow_none=True),
        "email": Email(allow_none=True)
    }), validate=lambda a: len(a) > 0),
    "created_at": DateTime(),
    "last_changed_at": DateTime(allow_none=True),
    "follows_id": Integer(allow_none=True),
    "attributes": Dict(),
    "archived": Boolean(),
    "no_change_time": Boolean(),
    "metadata": Dict(),
    "priority": Integer(load_default=0),
    "revision_n": Integer(),
    "edit_lock": Boolean(load_default=False)
}


class EntryResource(Resource):
    "Handle requests for a single entry"
    @use_args({"thread": Boolean()}, location="query")
    @marshal_with(fields.entry_full, envelope="entry")
    def get(self, args, entry_id, logbook_id=None, revision_n=None):
        entry = get_entry(entry_id, revision_n, args.get("thread", None))
        entry.logbook = get_logbook(entry.logbook.id)
        return entry

    @send_signal(new_entry)
    @use_args(entry_args, location='json')
    @marshal_with(fields.entry_full, envelope="entry")
    def post(self, args, logbook_id, entry_id=None):
        try:
            entry = create_entry(args, logbook_id, get_user(), entry_id)
            return entry
        except ValueError as e:
            abort(422, messages={"attributes": [str(e)]})

    @send_signal(edit_entry)
    @use_args(entry_args, location='json')
    @marshal_with(fields.entry_full, envelope="entry")
    def put(self, args, entry_id, logbook_id=None):
        "update entry"
        entry_id = entry_id or args["id"]

        # The revision number is needed to avoid overwrites
        if "revision_n" not in args:
            abort(400, message="Missing 'revision_n' field!")
        entry = update_entry(entry_id, request.environ.get('HTTP_X_FORWARDED_FOR', request.remote_addr), get_user(), **args)

        return entry


entries_args = {
    "title": Str(),
    "content": Str(),
    "authors": Str(),
    "attachments": Str(),
    "attribute": List(Str(validate=lambda s: len(s.split(":")) == 2)),
    "metadata": List(Str(validate=lambda s: len(s.split(":")) == 2)),
    "archived": Boolean(),
    "ignore_children": Boolean(),
    "n": Integer(load_default=50),
    "offset": Integer(),
    "download": Str(),
    "sort_by_timestamp": Boolean(load_default=True),
}


class EntriesResource(Resource):

    "Handle requests for entries from a given logbook, optionally filtered"

    @use_args(entries_args, location="query")
    def get(self, args, logbook_id=None):
        attributes = [attr.split(":") for attr in args.get("attribute", [])]
        metadata = [meta.split(":") for meta in args.get("metadata", [])]

        logbook = None
        if logbook_id:
            # restrict search to the given logbook and its descendants
            logbook = get_logbook(logbook_id)
        elif logbook_id == 0:
            # This NEED to return a logbook, if it's the root then just get all logbooks
            # Create an empty parent for the root
            logbook = dict(id=0, children=get_logbooks(logbook_id))

        search_args = dict(child_logbooks=not args.get("ignore_children"),
                           title_filter=args.get("title"),
                           content_filter=args.get("content"),
                           author_filter=args.get("authors"),
                           attachment_filter=args.get("attachments"),
                           attribute_filter=attributes,
                           metadata_filter=metadata,
                           n=args["n"], offset=args.get("offset"),
                           sort_by_timestamp=args.get("sort_by_timestamp"))
        entries = search_entry(logbook_id=logbook_id, **search_args)

        res = list(entries)

        return marshal(dict(logbook=logbook, entries=res), fields.entries)


class EntryLockResource(Resource):

    @marshal_with(fields.entry_lock, envelope="lock")
    def get(self, entry_id, logbook_id=None):
        "Check for a lock"
        return get_entry_lock(entry_id, ip=request.environ.get('HTTP_X_FORWARDED_FOR', request.remote_addr))

    @use_args({"steal": Boolean(load_default=False)}, location="json")
    @marshal_with(fields.entry_lock, envelope="lock")
    def post(self, args, entry_id, logbook_id=None):
        "Acquire (optionally stealing) a lock"
        return get_entry_lock(entry_id, ip=request.environ.get('HTTP_X_FORWARDED_FOR', request.remote_addr), acquire=True, steal=args["steal"])

    @use_args({"lock_id": Integer()}, location="query")
    @marshal_with(fields.entry_lock, envelope="lock")
    def delete(self, args, entry_id=None, logbook_id=None):
        "Cancel a lock"
        return cancel_entry_lock(request.environ.get('HTTP_X_FORWARDED_FOR', request.remote_addr), entry_id=entry_id, lock_id=args.get("lock_id", None))


class EntryChangesResource(Resource):

    @marshal_with(fields.entry_changes)
    def get(self, entry_id, logbook_id=None):
        changes = get_entry_changes(entry_id)
        return {"entry_changes": changes}


class EntryEditedResource(Resource):
    "Handles requests fetching updatetime of an entry"
    def get(self, entry_id):
        response = get_entry(entry_id)
        return str(response.last_changed_at)
