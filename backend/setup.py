from setuptools import setup

setup(
    name='elogy',
    author='Johan Forsberg',
    author_email='johan@slentrian.org',
    url='https://github.com/johanfforsberg/elogy',
    packages=['src'],
    include_package_data=True,
    install_requires=[
        'flask',
        'flask_restful',
        'webargs',
        'peewee',
        'blinker',
        'lxml',
        'pillow',
        'requests',
        'python-ldap',  # should be optional, depends on libpdap and libsasl!
        'wtf-peewee'
    ],
    extras_require={
        "dev": [
            "pytest",
            "faker",
            "splinter"
        ]
    }
)
