from operator import attrgetter

from faker import Faker

from src.service.entryservice import create_entry, update_entry, make_change_entry, get_entry_revision, search_entry
from src.service.logbookservice import create_logbook
from src.service.entrylockservice import get_entry_lock

from . import providers

# Entry

fake = Faker()
fake.add_provider(providers.ElogyProvider)


def test_entry_create(db):
    lb = create_logbook(fake.logbook())
    entry = fake.entry()
    title = entry.title
    en = create_entry(entry, logbook_id=lb.get_id())
    assert en.logbook == lb
    assert en.title == title


def test_entry_revision(db):
    ip = '0.0.0.0'
    lb = create_logbook(fake.logbook())
    en = create_entry(fake.entry(), logbook_id=lb.get_id())
    original = en.title
    get_entry_lock(en.get_id(), ip, acquire=True)
    revision = update_entry(en.get_id(), ip=ip, title='Entry2', revision_n=0)

    en.save()
    revision.save()

    assert len(en.changes) == 1
    rev = en.changes[0]
    assert revision == en
    assert rev.changed["title"] == original


def test_entry_revision_wrapper_1(db):
    lb = create_logbook(fake.logbook())
    entry = create_entry(fake.entry(title="Entry1"), logbook_id=lb.get_id())
    make_change_entry(entry, title="Entry2").save()
    entry.save()
    wrapper = get_entry_revision(entry, version=0)
    assert wrapper.revision_n == 0
    assert wrapper.change.changed['title'] == "Entry1"


def test_entry_revision_wrapper_2(db):
    lb = create_logbook(fake.logbook())

    entry_v0 = fake.entry()
    entry_v1 = fake.entry()
    entry_v2 = fake.entry()

    # create entry and modify it twice
    entry = create_entry(entry_v0, logbook_id=lb.get_id())
    make_change_entry(entry, **entry_v1).save()
    entry.save()
    make_change_entry(entry, **entry_v2).save()
    entry.save()

    # check that the wrapper reports the correct historical
    # values for each revision
    wrapper0 = get_entry_revision(entry, version=0)
    assert wrapper0.revision_n == 0
    assert wrapper0.title == entry_v0["title"]
    assert wrapper0.content == entry_v0["content"]

    wrapper1 = get_entry_revision(entry, version=1)
    assert wrapper1.revision_n == 1
    assert wrapper1.title == entry_v1["title"]
    assert wrapper1.content == entry_v1["content"]

    wrapper2 = get_entry_revision(entry, version=2)
    assert wrapper2.revision_n == 2
    assert wrapper2.title == entry_v2["title"]
    assert wrapper2.content == entry_v2["content"]


# Search
def prepare_entries_search():
    lb1 = create_logbook(fake.logbook(attributes=[
        {"options": [], "name": "a", "type": "number", "required": False},
        {"options": [], "name": "b", "type": "text", "required": False},
        {"options": [], "name": "c", "type": "multioption", "required": False},
        {"options": [], "name": "d", "type": "multioption", "required": False},
    ]))
    lb2 = create_logbook(fake.logbook(attributes=[
        {"options": [], "name": "a", "type": "number", "required": False},
        {"options": [], "name": "b", "type": "text", "required": False},
    ]))
    lb3 = create_logbook(fake.logbook())
    lb4 = create_logbook(fake.logbook(parent_id=lb1.get_id()))
    lb5 = create_logbook(fake.logbook(parent_id=lb4.get_id()))

    entries = {
        lb1.get_id(): [
            fake.entry(
                title="First Entry",
                content="This content is great!",
                attributes={"a": 1, "b": "2", "c": ["1", "2", "3"]},
                authors=[{"name": "alpha"}, {"name": "beta"}],
                metadata={"message": "hello"},
            ),
            fake.entry(
                title="Second Entry",
                content="Some very neat content.",
                attributes={"a": 1, "b": "3", "c": ["2"], "d": ["7"]},
                authors=[{"name": "alpha"}],
                metadata={"message": "yellow"},
            ),
            fake.entry(
                title="Third Entry",
                content="Not so bad content either.",
                attributes={"a": 2, "b": "2", "c": ["3", "4"]},
                authors=[{"name": "gamma"}, {"name": "beta"}],
                metadata={}
            ),
            fake.entry(
                title="Fourth Entry",
                content="Not so great content, should be ignored."
            ),
            fake.entry(
                title="First Followup to First Entry",
                content="Not so great content, should be ignored.",
                follows_id=0
            ),
            fake.entry(
                title="Second Followup to First Entry",
                content="Not so great content, should be ignored.",
                follows_id=0
            ),
            fake.entry(
                title="First Followup to Second Entry",
                content="Not so great content, should be ignored.",
                follows_id=1
            ),
        ],
        lb2.get_id(): [
            fake.entry(
                title="Fifth Entry",
                content="Not so great content. And in the wrong Logbook",
                attributes={"a": 1, "b": "2"},
                authors=[{"name": "gamma"}]
            ),
            fake.entry(
                title="First Followup to Fifth Entry",
                content="Not so great content. And in the wrong Logbook",
                follows_id=7
            ),
        ],
        lb3.get_id(): [
            fake.entry(
                title="Fifth Entry",
                content="Not so great content. And in the wrong Logbook",
                attributes={"a": 1},
            ),
        ],
        lb4.get_id(): [
            fake.entry(
                title="Child Logbook Entry",
                content="This content is great in a child logbook",
            ),
        ],
        lb5.get_id(): [
            fake.entry(
                title="Grandchild Logbook First Entry",
                content="Other stuff.",
            ),
            fake.entry(
                title="Grandchild Logbook Second Entry",
                content="This content is also great and in a grandchild logbook this time.",
            ),
        ]
    }

    # create entries
    for lb in entries:
        for entry in entries[lb]:
            create_entry(entry, lb)

    return lb1.get_id()


def test_entry_content_search(elogy, db):
    lb = prepare_entries_search()

    # simple search
    results = search_entry(logbook_id=lb, content_filter="great", child_logbooks=False)
    assert set(map(attrgetter("title"), results)) == {'First Entry',
                                                      'First Followup to First Entry',
                                                      'First Followup to Second Entry',
                                                      'Fourth Entry',
                                                      'Second Followup to First Entry'}

    # regexp search
    results = search_entry(logbook_id=lb, content_filter="Not.*content", child_logbooks=False)
    assert set(map(attrgetter("title"), results)) == {'First Followup to First Entry',
                                                      'First Followup to Second Entry',
                                                      'Fourth Entry',
                                                      'Second Followup to First Entry',
                                                      'Third Entry'}


def test_entry_content_search_global(elogy, db):
    lb1 = prepare_entries_search()
    # global search
    # simple search
    results = search_entry(content_filter="great")
    assert set(map(attrgetter("title"), results)) == {'Child Logbook Entry',
                                                      'Fifth Entry',
                                                      'First Entry',
                                                      'First Followup to Fifth Entry',
                                                      'First Followup to First Entry',
                                                      'First Followup to Second Entry',
                                                      'Fourth Entry',
                                                      'Grandchild Logbook Second Entry',
                                                      'Second Followup to First Entry'}

    # regexp search
    results = search_entry(content_filter="Not.*content")
    assert set(map(attrgetter("title"), results)) == {'Fifth Entry',
                                                      'First Followup to Fifth Entry',
                                                      'First Followup to First Entry',
                                                      'First Followup to Second Entry',
                                                      'Fourth Entry',
                                                      'Second Followup to First Entry',
                                                      'Third Entry'}


def test_entry_title_search(elogy, db):
    lb = prepare_entries_search()

    # simple search
    results = search_entry(logbook_id=lb, title_filter="First Entry")
    assert set(map(attrgetter("title"), results)) == {'First Entry',
                                                      'First Followup to First Entry',
                                                      'Second Followup to First Entry'}

    # regexp search
    results = search_entry(logbook_id=lb, title_filter="F.*ry")
    assert set(map(attrgetter("title"), results)) == {'First Entry',
                                                      'First Followup to First Entry',
                                                      'First Followup to Second Entry',
                                                      'Fourth Entry',
                                                      'Second Followup to First Entry'}


def test_entry_search_followups(elogy, db):
    lb = prepare_entries_search()

    # simple search
    results = search_entry(logbook_id=lb)
    assert len(results) == 4

    results = search_entry(logbook_id=lb, followups=True)
    assert len(results) == 7

    results = search_entry(logbook_id=lb, title_filter="First Entry")
    assert set(map(attrgetter("title"), results)) == {"First Entry",
                                                      "First Followup to First Entry",
                                                      "Second Followup to First Entry"}


def test_entry_attribute_search_followups(elogy, db):
    lb = prepare_entries_search()

    # simple search
    results = search_entry(logbook_id=lb, followups=True, attribute_filter=[("a", 1)])
    assert set(map(attrgetter("title"), results)) == {'First Entry', 'Second Entry'}

    results = search_entry(followups=True, attribute_filter=[("a", 1)])
    assert set(map(attrgetter("title"), results)) == {'First Entry', "Second Entry", "Fifth Entry"}


def test_entry_authors_search(elogy, db):
    lb = prepare_entries_search()

    results = search_entry(logbook_id=lb, author_filter="alpha")
    assert set(map(attrgetter("title"), results)) == {"First Entry", "Second Entry"}

    # either
    results = search_entry(logbook_id=lb, author_filter="alpha|beta")
    assert set(map(attrgetter("title"), results)) == {"First Entry", "Second Entry", "Third Entry"}


def test_entry_attribute_filter(elogy, db):
    lb = prepare_entries_search()

    # filter attributes
    results = search_entry(logbook_id=lb, attribute_filter=[("a", 2)])
    assert set(map(attrgetter("title"), results)) == {"Third Entry"}

    results = search_entry(logbook_id=lb, attribute_filter=[("b", "2")])
    assert set(map(attrgetter("title"), results)) == {"First Entry", "Third Entry"}

    # multiple attribute filter
    results = search_entry(logbook_id=lb, attribute_filter=[("a", 2), ("b", "2")])
    assert set(map(attrgetter("title"), results)) == {"Third Entry"}


def test_entry_attribute_multi_option_filter(elogy, db):
    lb = prepare_entries_search()

    # filter attributes
    results = search_entry(logbook_id=lb, attribute_filter=[("c", "1")])
    assert set(map(attrgetter("title"), results)) == {"First Entry"}

    # one value matching several entries
    results = search_entry(logbook_id=lb, attribute_filter=[("c", "2")])
    assert set(map(attrgetter("title"), results)) == {"First Entry", "Second Entry"}

    # two values for one attribute
    results = search_entry(logbook_id=lb, attribute_filter=[("c", "2"), ("c", "3")])
    assert set(map(attrgetter("title"), results)) == {"First Entry"}

    # two different attributes
    results = search_entry(logbook_id=lb, attribute_filter=[("c", "2"), ("d", "7")])
    assert set(map(attrgetter("title"), results)) == {"Second Entry"}


def test_entry_metadata_filter(elogy, db):
    lb = prepare_entries_search()

    # filter attributes
    results = search_entry(logbook_id=lb, metadata_filter=[("message", "hello")])
    assert set(map(attrgetter("title"), results)) == {"First Entry"}

    results = search_entry(logbook_id=lb, metadata_filter=[("message", ".*ello.*")])
    assert set(map(attrgetter("title"), results)) == {"First Entry", "Second Entry"}


def test_entry_content_search_child_logbooks(elogy, db):
    lb = prepare_entries_search()

    # only parent logbook
    results = list(search_entry(logbook_id=lb, child_logbooks=False, content_filter="content.*great"))
    assert set(map(attrgetter("title"), results)) == {'First Entry'}

    # include child logbooks
    results = list(search_entry(logbook_id=lb, child_logbooks=True, content_filter="content.*great"))
    assert set(map(attrgetter("title"), results)) == {'First Entry',
                                                      'Child Logbook Entry',
                                                      'Grandchild Logbook Second Entry'}

    # more restrictive
    results = list(search_entry(logbook_id=lb, child_logbooks=True, content_filter="neat content"))
    assert set(map(attrgetter("title"), results)) == {'Second Entry'}
