from random import randint

from faker import Faker
from faker.providers import BaseProvider


fake = Faker()


class DotDict(dict):
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__


class ElogyProvider(BaseProvider):

    @staticmethod
    def title():
        return fake.sentence()

    @staticmethod
    def authors():
        authors = []
        for i in range(randint(1, 5)):
            name = fake.name()
            first, last = name.split(" ", 1)
            login = (first[:3] + last[:3]).lower()
            email = fake.email()
            authors.append(dict(name=name, email=email, login=login))
        return authors

    @staticmethod
    def text_content():
        return fake.text()

    @staticmethod
    def html_content():
        body = "</p><p>".join(fake.paragraphs())
        return "<html><body><p>{}</p></body></html>".format(body)

    @staticmethod
    def add_extra_kwargs(item, extra):
        for key in extra:
            item[key] = extra[key]
        return item

    def logbook(self, raw=False, **kwargs):
        logbook = {
            "name": self.title(),
            "description": self.html_content()
        }
        if kwargs:
            logbook = self.add_extra_kwargs(logbook, kwargs)

        if raw:
            return logbook
        return DotDict(logbook)

    def entry(self, plaintext=False, **kwargs):
        entry = {
            "title": self.title(),
            "authors": self.authors(),
            "content_type": "text/html",
            "content": self.html_content()
        }
        if plaintext:
            entry["content_type"] = "text/plain"
        if kwargs:
            entry = self.add_extra_kwargs(entry, kwargs)
        return DotDict(entry)
