import os
import re

TITLE = os.getenv('ELOGY_TITLE', 'elogy')

BASEURL = os.getenv('ELOGY_URL', 'https://elogy.maxiv.lu.se')

# The name of the database file
DB_TYPE = os.getenv('ELOGY_DB_TYPE', 'sqlite')  # [postgresql, sqlite]
DB_NAME = os.getenv('ELOGY_DB_NAME', '/tmp/elogy.db')  # !!!Do not use /tmp for anything beyond testing!!!
DB_HOST = os.getenv('ELOGY_DB_HOST', '')
DB_USER = os.getenv('ELOGY_DB_USER', '')
DB_PASS = os.getenv('ELOGY_DB_PASS', '')

LOGGING_LEVEL = os.getenv('LOGGING_LEVEL', 'WARNING')  # CRITICAL, ERROR, WARNING, INFO, DEBUG

# The secret used by python in sessions
SECRET = os.getenv('ELOGY_SECRET', 'RandomSecret')

JWT_DECODE_URL = os.getenv('JWT_DECODE_URL', 'https://auth.maxiv.lu.se/v1/decode')  
JWT_AUTH_URL = os.getenv('JWT_AUTH_URL', 'https://auth.maxiv.lu.se/v1/login')

# The folder where all uploaded files will be stored.
UPLOAD_FOLDER = os.getenv('ELOGY_UPLOAD_FOLDER', '/tmp/elogy')  # !!!Again, /tmp is a bad choice!!!

# Optional LDAP config. Used to autocomplete author names.
# Requires the "pyldap" package. If not set, elogy will try
# to fall back to looking up users through the local system.
LDAP_SERVER = os.getenv("ELOGY_LDAP_SERVER", "")
LDAP_BASEDN = os.getenv("ELOGY_LDAP_BASEDN", "")
LDAP_BIND_USERNAME = os.getenv("ELOGY_LDAP_BIND_USERNAME", "")
LDAP_BIND_PASSWORD = os.getenv("ELOGY_LDAP_BIND_PASSWORD", "")

LDAP_USERNAME_ATTRIBUTE = "sAMAccountName"


# Callbacks for various events
def new_entry(data):
    """Gets run whenever a new entry has been created"""

    entry = data["entry"]

    # 'entry' is the entry we just created, so it contains all the
    # data, e.g. title, authors, content, attributes and so on.
    # It's *not* the actual db model object, it's serialized
    # into a dict (same format as the JSON api). We don't want user
    # scripts to accidentally modify the database...

    # Should be OK to do potentially slow stuff here such as network
    # calls, since actions are run in a thread. But make sure it
    # terminates sooner or later or you will have a resource leak!

    # Some example actions:
    if "Mailto" in entry["attributes"]:

        # Email the address(es) given in the "Mailto" attribute

        # TODO: inline images aren't displayed since they are just
        #       relative links. Might be a good idea to inline them.
        # TODO: shouldn't assume messages are HTML
        # TODO: include entry authorship information?

        import logging
        from smtplib import SMTP
        from email.mime.text import MIMEText
        if "Mailfrom" in entry["attributes"]:
            from_addr = entry["attributes"]["Mailfrom"]
        else:
            from_addr = "elogy@maxiv.lu.se"

        toaddrs = entry["attributes"]["Mailto"]
        if isinstance(toaddrs, list):
            toaddrs = ",".join(toaddrs)
        link_to_text = 'Sent from Elogy, original post can be found '
        link_to_entry = BASEURL + '/logbooks/' + str(entry["logbook"]["id"]) + '/entries/' + str(entry["id"])
        content = "<html> {} <br> {} </html>".format(entry["content"],
                                                     link_to_text + "<a href='" + link_to_entry + "'>here</a>")
        content = re.sub('src="/attachments/', 'src="' + BASEURL + "/attachments/", content)
        content = re.sub('href="/attachments/', 'href="' + BASEURL + "/attachments/", content)
        message = MIMEText(content, "html")
        message["Subject"] = entry["title"]
        message["From"] = from_addr
        message["To"] = toaddrs
        with SMTP("smtp.maxiv.lu.se", 25) as smtp:
            logging.info(smtp.send_message(message))

    if "Ticket" in entry["attributes"]:
        # Create a ticket in your ticket system
        pass


def edit_logbook(data):
    print("edit_logbook", data)


ACTIONS = {
    "new_entry": new_entry,
    "edit_entry": None,
    "new_logbook": None,
    "edit_logbook": None  # edit_logbook
}
